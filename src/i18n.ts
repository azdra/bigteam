import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';

const translationGetters = {
  en: () => require('./translations/en.json'),
};

i18n.use(initReactI18next).init({
  compatibilityJSON: 'v3',
  resources: {
    en: {
      translation: translationGetters['en'](),
    },
  },
  lng: 'en',
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false,
  },
});
