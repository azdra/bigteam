import axios, {Method} from 'axios';

const makeRequest = async (url: string, method: Method = 'GET', data?: any): Promise<any> => {
  const headers = {
    'accept': 'application/json',
    'content-type': 'application/json',
  };

  return new Promise((resolve, reject) => {
    return axios({
      headers,
      url,
      method,
      data,
      baseURL: 'https://project-expo-back.herokuapp.com',
    })
        .then((r) => resolve(r.data))
        .catch((e) => reject(e));
  });
};

export default makeRequest;
