import makeRequest from './makeRequest';
import {Project} from '../types/Project';

export const createProject = async (data: any): Promise<Project> =>
  await makeRequest('/projects', 'POST', data);

export const getProjects = async (): Promise<Project[]> =>
  await makeRequest('/projects');

export const getProject = async (id: number): Promise<Project> =>
  await makeRequest('/projects/'+ id);

export const updateProject = async (id: number, data: any): Promise<Project> =>
  await makeRequest('/projects/'+ id, 'PUT');

export const deleteProject = async (id: number): Promise<Project> =>
  await makeRequest('/projects/'+ id, 'DELETE');
