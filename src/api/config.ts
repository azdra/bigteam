const api_domain = process.env.API_URL;

export const api = {
  domain: api_domain,
  routes: {
    authentication: '/auth/login',
    users: {
      base: '/user',
      account: '/account',
    },
  },
};
