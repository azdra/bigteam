import React from 'react';
import {Image, View, Text, StyleSheet} from 'react-native';
import {useTranslation} from 'react-i18next';

const Logo = () => {
  const {t} = useTranslation();

  return (
    <View style={styles.view}>
      <Image style={styles.image} source={require('../../../assets/logo.png')}/>
      <Text style={styles.text}>{t('LOGO_TITLE')}</Text>
    </View>
  );
};

export default Logo;

const styles = StyleSheet.create({
  view: {
    padding: 10,
  },
  image: {
    width: 117,
    height: 50,
  },
  text: {
    color: '#FFFFFF',
  },
});
