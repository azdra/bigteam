import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';
import {useTheme} from '@react-navigation/native';

const Loading = () => {
  const theme = useTheme();

  return (
    <View style={[styles.container, styles.horizontal]}>
      <ActivityIndicator size={60} color={theme.colors.primary}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
});


export default Loading;
