import React, {useEffect} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import CustomButton from '../Button/Button';

const NoProjectFound = () => {
  const navigation = useNavigation();
  const theme = useTheme();

  useEffect(() => {
    navigation.setOptions({
      headerTitle: 'Project not found!',
    });
  }, []);


  return (
    <View style={style.container}>
      <Text style={style.title}>Project not found</Text>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Text style={[style.subTitle, {backgroundColor: theme.colors.primary}]}>Go back</Text>
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 18,
  },
  subTitle: {
    color: 'white',
    padding: 10,
    borderRadius: 5,
    paddingHorizontal: 20,
    marginTop: 10,
    fontSize: 16,
  },
});

export default NoProjectFound;
