import {
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';
import React, {FC} from 'react';
import {useTheme} from '@react-navigation/native';
import {CustomButton as CustomButtonType} from '../../types/CustomButton';

const CustomButton: FC<CustomButtonType> = (props: CustomButtonType) => {
  const theme = useTheme();
  const colorScheme = useColorScheme();

  return (
    // @ts-ignore
    <TouchableOpacity onPress={props.onPress}>
      <View style={[{backgroundColor: theme.colors.primary}, styles.container]}>
        {
          props.icon && <props.icon.type
            style={styles.icon}
            name={props.icon.name}
            size={props.icon.size}
            color={colorScheme === 'light' ? '#FFF' : '#000'}
          />
        }
        <Text
          style={[
            {
              color: colorScheme === 'light' ? '#FFF' : '#000',
            },
            styles.text,
          ]}
        >
          {props.text}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
    alignSelf: 'stretch',
  },
  icon: {
    marginRight: 10,
  },
  text: {
    fontSize: 16,
  },
});

export default CustomButton;
