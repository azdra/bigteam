import React, {FC} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Task, taskKeyToName} from '../../types/Task';
import dayjs from 'dayjs';

const TaskCard: FC<Task> = (props: Task) => {
  return (
    <TouchableOpacity>
      <View style={styles.container}>
        <Text style={styles.title}>{props.name}</Text>
        <View>
          <Text>Created on: {dayjs(props.created_at).format('DD/MM/YYYY')}</Text>
          <Text>Status: {taskKeyToName[props.status] ?? 'No status'}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.3,
    shadowRadius: 16.00,
    elevation: 3,

    margin: 10,
    marginVertical: 7,
    padding: 12,
    borderRadius: 5,
    backgroundColor: '#ffffff',
    flex: 1,
  },
  deleteButton: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#dc3545',
  },
  title: {
    flex: 1,
    fontSize: 20,
    fontWeight: 'bold',
  },
  infoContainer: {
    marginTop: 10,
  },
  info: {
    fontSize: 14,
    color: '#777',
  },
});

export default TaskCard;
