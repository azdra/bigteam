import React, {FC} from 'react';
import {Project} from '../../types/Project';
import {Alert, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AntDesign} from '@expo/vector-icons';
import {useTranslation} from 'react-i18next';
import {deleteProject} from '../../api/project';
import {useMutation} from 'react-query';
import {useNavigation} from '@react-navigation/native';

const ProjectCard: FC<Project> = (props: Project) => {
  const {name, tasks, id} = props;
  const {t} = useTranslation();
  const navigation = useNavigation<any>();

  const mutation = useMutation((id: number) => {
    return deleteProject(id);
  });

  const confirmDeleteProject = () => {
    return Alert.alert(
        t('PROJECT_DELETE_TITLE'),
        t('PROJECT_DELETE_SUBTITLE', {
          'PROJECT_NAME': name,
        }),
        [
          {
            text: 'Yes',
            onPress: () => mutation.mutate(id),
          },
          {
            text: 'No',
          },
        ],
    );
  };

  return (
    <TouchableOpacity onPress={() => navigation.navigate('ProjectDetail', {
      id: props.id,
    })}>
      <View style={styles.container}>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          <Text style={styles.title}>{name}</Text>
          <View>
            <TouchableOpacity
              onPress={confirmDeleteProject}
              style={styles.deleteButton}
            >
              <AntDesign name="delete" size={16} color="white" />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.infoContainer}>
          <Text style={styles.info}>Number of task: {tasks.length}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.3,
    shadowRadius: 16.00,
    elevation: 3,

    margin: 10,
    marginVertical: 7,
    padding: 12,
    borderRadius: 5,
    backgroundColor: '#ffffff',
    flex: 1,
  },
  deleteButton: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 100,
    backgroundColor: '#dc3545',
  },
  title: {
    flex: 1,
    fontSize: 24,
    fontWeight: '700',
  },
  infoContainer: {
    marginTop: 10,
  },
  info: {
    fontSize: 14,
    color: '#777',
  },
});

export default ProjectCard;
