import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import './i18n';
import Navigation from './navigation';
import useCachedResources from './hook/useCachedResources';
import useColorScheme from './hook/useColorScheme';
import {QueryClient, QueryClientProvider} from 'react-query';
const queryClient = new QueryClient();
import {LogBox} from 'react-native';
import Loading from './component/Loading/Loading';
LogBox.ignoreLogs(['Setting a timer']);

const Application = () => {
  const isLoadingComplete = useCachedResources();
  const colorScheme = useColorScheme();

  if (!isLoadingComplete) {
    return <Loading/>;
  } else {
    return (
      <SafeAreaProvider>
        <QueryClientProvider client={queryClient}>
          <Navigation colorScheme={colorScheme} />
        </QueryClientProvider>
      </SafeAreaProvider>
    );
  }
};

export default Application;
