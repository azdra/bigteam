import {RootTabScreenProps} from '../../types';
import {Controller, useForm} from 'react-hook-form';
import {StyleSheet, TextInput, View, Text, TouchableOpacity, ScrollView, ActivityIndicator} from 'react-native';
import * as React from 'react';
import {useTranslation} from 'react-i18next';
import CustomButton from '../../component/Button/Button';
import {useTheme} from '@react-navigation/native';
import {createProject} from '../../api/project';
import {useMutation} from 'react-query';

type FormProjectData = {
  name: string;
  description: string;
  expected_duration: string;
};

const CreateStepModal = ({navigation}: RootTabScreenProps<'CreateProjectModal'>) => {
  const goBackTimeOut = 3;
  const {t} = useTranslation();
  const theme = useTheme();

  const {control, handleSubmit, formState: {errors}} = useForm<FormProjectData>({
    defaultValues: {
      name: '',
      description: '',
      expected_duration: '',
    },
  });

  const mutation = useMutation((data: FormProjectData) => {
    return createProject(data).then(() => {
      setTimeout(() => {
        navigation.goBack();
      }, goBackTimeOut*1000);
    });
  });

  const onSubmit = handleSubmit((data: FormProjectData) => {
    return mutation.mutate(data);
  });

  return (
    <View style={styles.container}>

      {
        (mutation.isLoading || mutation.isSuccess) ? <View style={styles.successContainer}>
          {
            mutation.isLoading && <ActivityIndicator size={32} color={theme.colors.primary}/>
          }
          {
            mutation.isSuccess ? <View style={{marginVertical: 10}}>
              <Text style={{textAlign: 'center', fontSize: 16, fontWeight: 'bold'}}>Project successfully created!</Text>
              <Text style={
                {textAlign: 'center',
                  fontSize: 14,
                  marginVertical: 2}
              }>
                Back in {goBackTimeOut} seconds
              </Text>
            </View> : null
          }
        </View> : null
      }

      {
        (!mutation.isSuccess || !mutation.isSuccess) ? <View style={styles.container}>
          <ScrollView>
            <View>
              <Controller
                control={control}
                rules={{
                  required: true,
                  maxLength: 100,
                }}
                render={({field: {onChange, onBlur, value}}) => (
                  <View style={styles.containerInput}>
                    <Text style={styles.inputLabel}>{t('STEP_NAME')+' :*'}</Text>
                    <TextInput
                      placeholder={t('STEP_NAME')}
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={onChange}
                      value={value}
                    />
                  </View>
                )}
                name="name"
              />
              {
                errors.name && <Text style={styles.errorField}>This is required.</Text>
              }
            </View>

            <View>
              <Controller
                control={control}
                rules={{
                  required: true,
                  maxLength: 100,
                }}
                render={({field: {onChange, onBlur, value}}) => (
                  <View style={styles.containerInput}>
                    <Text style={styles.inputLabel}>{t('STEP_DESCRIPTION')+' :*'}</Text>
                    <TextInput
                      placeholder={t('STEP_DESCRIPTION')}
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={onChange}
                      value={value}
                    />
                  </View>
                )}
                name="description"
              />
              {
                errors.description && <Text style={styles.errorField}>This is required.</Text>
              }
            </View>

            <View>
              <Controller
                control={control}
                rules={{
                  required: true,
                  maxLength: 100,
                }}
                render={({field: {onChange, onBlur, value}}) => (
                  <View style={styles.containerInput}>
                    <Text style={styles.inputLabel}>{t('STEP_DURATION')+' :*'}</Text>
                    <TextInput
                      placeholder={t('STEP_DURATION')}
                      style={styles.input}
                      onBlur={onBlur}
                      onChangeText={onChange}
                      value={value}
                    />
                  </View>
                )}
                name="expected_duration"
              />
              {
                errors.expected_duration && <Text style={styles.errorField}>This is required.</Text>
              }
            </View>
          </ScrollView>

          <CustomButton text={'Submit'} onPress={onSubmit}/>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Text style={[{color: theme.colors.primary}, styles.dismiss]}>Dismiss</Text>
          </TouchableOpacity>
        </View> : null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  successContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  errorField: {
    color: 'red',
    marginHorizontal: 5,
  },
  container: {
    flex: 1,
  },
  containerInput: {
    margin: 10,
  },
  inputLabel: {
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 5,
    marginBottom: 5,
  },
  input: {
    borderWidth: 1,
    borderColor: 'rgba(0, 0, 0, .1)',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#ffffff',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 12,
    },
    shadowOpacity: 0.3,
    shadowRadius: 10.00,
    elevation: 5,
  },
  dismiss: {
    backgroundColor: 'rgba(200, 200, 200, .5)',
    padding: 5,
    fontSize: 14,
    textAlign: 'center',
  },
});

export default CreateStepModal;
