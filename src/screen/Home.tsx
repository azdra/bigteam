import {
  ScrollView,
  StyleSheet,
  View,
} from 'react-native';
import React from 'react';
import {RootTabScreenProps} from '../types';
import {getProjects} from '../api/project';
import {useQuery} from 'react-query';
import ProjectCard from '../component/ProjectCard/ProjectCard';
import Loading from '../component/Loading/Loading';
import {useTranslation} from 'react-i18next';
import {FontAwesome} from '@expo/vector-icons';
import CustomButton from '../component/Button/Button';

const Home = ({navigation}: RootTabScreenProps<'Home'>) => {
  const {t} = useTranslation();

  const {isLoading, data: projects} = useQuery('projects', getProjects, {
    cacheTime: 1,
  });

  if (isLoading) {
    return <Loading/>;
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        {
          projects && projects.map((p, i) => {
            return <ProjectCard key={i} {...p}/>;
          })
        }
      </ScrollView>

      <View style={{marginHorizontal: 10, marginBottom: 10}}>
        <CustomButton onPress={() => {
          navigation.navigate('CreateProjectModal');
        }} text={t('ADD_PROJECT')} icon={{
          type: FontAwesome,
          name: 'plus',
          size: 25,
        }}/>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default Home;
