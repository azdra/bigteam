import {ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {RootTabScreenProps} from '../types';
import {useQuery} from 'react-query';
import {getProject} from '../api/project';
import Loading from '../component/Loading/Loading';
import CustomButton from '../component/Button/Button';
import {FontAwesome} from '@expo/vector-icons';
import NoProjectFound from '../component/NoProjectFound/NoProjectFound';
import TaskCard from '../component/TaskCard/TaskCard';

const ProjectDetail = ({route, navigation}: RootTabScreenProps<'ProjectDetail'>) => {
  const {id} = route.params;
  const {isLoading, data: project} = useQuery('project', () => getProject(id));

  if (isLoading) {
    return <Loading/>;
  }

  if (!project) {
    return <NoProjectFound/>;
  }

  return (
    <View style={styles.container}>
      <ScrollView>
        <View style={{marginHorizontal: 10, marginVertical: 10}}>
          <Text style={styles.title}>{project.name}</Text>
          <View style={styles.overview}>
            <Text style={styles.subTitle}>{project.description}</Text>
            <Text>- Begin date</Text>
            <Text>- Dead line</Text>
            <Text>- Total time left</Text>
          </View>
        </View>

        <View>
          {
            project.tasks.map((task, i) => {
              return <TaskCard key={i} {...task}/>;
            })
          }
        </View>
      </ScrollView>

      <View style={{marginHorizontal: 10, marginBottom: 10}}>
        <CustomButton onPress={() => {
          navigation.navigate('CreateStepModal');
        }} text={'Add a step'} icon={{
          type: FontAwesome,
          name: 'plus',
          size: 25,
        }}/>
      </View>
    </View>
  );
};

export default ProjectDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  overview: {
    marginTop: 20,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
  },
  subTitle: {
    marginBottom: 10,
    fontSize: 17,
    fontWeight: 'bold',
  },
});
