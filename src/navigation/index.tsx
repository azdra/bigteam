import React from 'react';
import {ColorSchemeName} from 'react-native';
import {NavigationContainer, useTheme} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from '../screen/Home';
import ProjectDetail from '../screen/ProjectDetail';
import {RootStackParamList} from '../types';
import Logo from '../component/Logo/Logo';
import {DarkTheme, LightTheme} from '../theme';
import {StatusBar} from 'expo-status-bar';
import {Fragment} from 'react';
import CreateProjectModal from '../screen/Modal/CreateProjectModal';
import CreateStepModal from '../screen/Modal/CreateStepModal';

const Navigation =({colorScheme}: { colorScheme: ColorSchemeName }) => {
  return (
    <Fragment>
      <StatusBar style={colorScheme === 'dark' ? 'dark' : 'light'} />
      <NavigationContainer
        theme={colorScheme === 'dark' ? DarkTheme : LightTheme}>
        <RootNavigator />
      </NavigationContainer>
    </Fragment>
  );
};
export default Navigation;

const Stack = createNativeStackNavigator<RootStackParamList>();

const RootNavigator = () => {
  const theme = useTheme();
  return (
    <Stack.Navigator screenOptions={{
      headerTintColor: '#fff',
      headerStyle: {
        backgroundColor: theme.colors.primary,
      },
    }} initialRouteName={'Home'}>
      <Stack.Group>
        <Stack.Group>
          <Stack.Screen name="Home" component={Home} options={{
            headerTitle: (props) => <Logo />,
            headerTitleAlign: 'center',
            headerStyle: {
              backgroundColor: theme.colors.primary,
            },
          }} />
          <Stack.Group screenOptions={{presentation: 'modal'}}>
            <Stack.Screen
              name="CreateProjectModal"
              component={CreateProjectModal}
              options={{
                title: 'Create new project',
              }} />
          </Stack.Group>
        </Stack.Group>

        <Stack.Group>
          <Stack.Screen
            name="ProjectDetail"
            component={ProjectDetail}
            options={{
              title: 'Project detail',
            }}
          />
          <Stack.Group screenOptions={{presentation: 'modal'}}>
            <Stack.Screen
              name="CreateStepModal"
              component={CreateStepModal}
              options={{
                title: 'Create new step',
              }} />
          </Stack.Group>
        </Stack.Group>
      </Stack.Group>
    </Stack.Navigator>
  );
};
