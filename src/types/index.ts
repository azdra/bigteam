import {NativeStackScreenProps} from '@react-navigation/native-stack';

export type RootStackParamList = {
  Home: undefined;
  ProjectDetail: {
    id: number
  };
  TaskDetail: undefined;
  CreateProjectModal: undefined;
  CreateStepModal: undefined;
};

export type RootTabScreenProps<Screen extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, Screen>;
