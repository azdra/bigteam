export type taskKeyToNameType = {
  to_do: string,
  in_progress: string,
  to_test: string,
  to_end: string,
}

export const taskKeyToName: taskKeyToNameType = {
  to_do: 'To Do',
  in_progress: 'In Progress',
  to_test: 'To Test',
  to_end: 'Finish',
};

export type Task = {
  id: number;
  actual_duration: any;
  created_at: any;
  description: any;
  expected_duration: any;
  name: string;
  project: any;
  published_at: any;
  status: keyof taskKeyToNameType;
  updated_at?: any;
  user?: any;
}
