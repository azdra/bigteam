export type CustomButton = {
  text: string;
  onPress: any;
  icon?: {
    type: Function;
    name: string;
    size: number;
  }
}
