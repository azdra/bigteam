export type Project = {
  id: number;
  name: string;
  description: string;
  tasks: any[];
  team: any[];
  created_at: Date;
  published_at: Date;
  updated_at: Date;
  start_date?: Date;
  due_date?: Date;
  end_date?: Date;
}
